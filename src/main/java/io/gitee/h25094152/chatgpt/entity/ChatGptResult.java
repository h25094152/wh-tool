package io.gitee.h25094152.chatgpt.entity;


import java.util.List;

/**
 * chatGPT结果集
 */

public class ChatGptResult {

    private Integer created;

    private String model;

    private String id;

    private String object;

    private List<ChatGptChoice> choices;

    public Integer getCreated() {
        return created;
    }

    public void setCreated(Integer created) {
        this.created = created;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public List<ChatGptChoice> getChoices() {
        return choices;
    }

    public void setChoices(List<ChatGptChoice> choices) {
        this.choices = choices;
    }

    @Override
    public String toString() {
        return "ChatGptResult{" +
                "created=" + created +
                ", model='" + model + '\'' +
                ", id='" + id + '\'' +
                ", object='" + object + '\'' +
                ", choices=" + choices +
                '}';
    }
}
