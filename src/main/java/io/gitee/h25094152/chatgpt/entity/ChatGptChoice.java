package io.gitee.h25094152.chatgpt.entity;


/**
 * chatGPT结果集
 */

public class ChatGptChoice {

    private String finishReason;

    private Integer index;

    private String text;

    private ChatGPTMessage message;

    @Override
    public String toString() {
        return "ChatGptChoice{" +
                "finishReason='" + finishReason + '\'' +
                ", index=" + index +
                ", text='" + text + '\'' +
                ", message=" + message +
                '}';
    }

    public String getFinishReason() {
        return finishReason;
    }

    public void setFinishReason(String finishReason) {
        this.finishReason = finishReason;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ChatGPTMessage getMessage() {
        return message;
    }

    public void setMessage(ChatGPTMessage message) {
        this.message = message;
    }
}
