package io.gitee.h25094152.chatgpt.entity;


/**
 * chatGpt 请求实体
 */

public class ChatGptParam {

    /**
     * 问题
     */
    private String question;

    /**
     * 回答
     */
    private String answer;

    ChatGptParam(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }


    public static ChatGptParam.ChatGptParamBuilder builder() {
        return new ChatGptParam.ChatGptParamBuilder();
    }

    public static class ChatGptParamBuilder {
        private String question;

        /**
         * 回答
         */
        private String answer;

        ChatGptParamBuilder() {
        }

        public ChatGptParam.ChatGptParamBuilder question(String question) {
            this.question = question;
            return this;
        }

        public ChatGptParam.ChatGptParamBuilder answer(String answer) {
            this.answer = answer;
            return this;
        }

        public ChatGptParam build() {
            return new ChatGptParam(this.question, this.answer);
        }

        @Override
        public String toString() {
            return "ChatGptParamBuilder{" +
                    "question='" + question + '\'' +
                    ", answer='" + answer + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ChatGptParam{" +
                "question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                '}';
    }
}
