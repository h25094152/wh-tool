package io.gitee.h25094152.crypto.rsa;

import java.io.*;
import java.util.Scanner;

/**
 * @author wh
 * 生成的密钥转成Java可用
 */
public class CpuSN {

    public static String getCpuSN2() throws IOException {
        Process process = Runtime.getRuntime().exec(
                new String[]{"wmic", "cpu", "get", "ProcessorId"});
        process.getOutputStream().close();
        Scanner sc = new Scanner(process.getInputStream());
        String property = sc.next();
        String serial = sc.next();
        return serial;
    }

    public static String getCpuSN() {
        String drive = "C:/";
        String result = "";
        try {
            File file = File.createTempFile("tmp_02", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new FileWriter(file);

            String vbs = "Set objFSO = CreateObject(\"Scripting.FileSystemObject\")\n"
                    + "Set colDrives = objFSO.Drives\n"
                    + "Set objDrive = colDrives.item(\""
                    + drive
                    + "\")\n"
                    + "Wscript.Echo objDrive.SerialNumber";
            fw.write(vbs);
            fw.close();
            Process p = Runtime.getRuntime().exec(
                    "cscript //NoLogo " + file.getPath());
            BufferedReader input = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();
            file.delete();
        } catch (Exception e) {
        }
        if (result.trim().length() < 1 || result == null) {
            result = "";
        }
        return result.trim();
    }

    public static void main(String[] args) {
        String cpuSN = getCpuSN();
        System.out.println("cpuSN = " + cpuSN);
    }
}
