
package io.gitee.h25094152.crypto.rsa;

import io.gitee.h25094152.date.DateUtil;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @author wh
 */
public class LicenseHelper {

    private static String outDateString = "";
    public String errorMsg = "";
    public String successMsg = "";

    public static void main(String[] args) {
        LicenseHelper licenseHelper = new LicenseHelper();
        boolean verify = licenseHelper.verify();
        System.out.println("verify = " + verify);
    }

    public String GetSn() {
        return CpuSN.getCpuSN();
    }




    public String convLanguage(String context){
        return convLanguage(context,"ZH-CN");
    }

    /**
     * 语言转換器
     * @param context
     * @param type
     * @return
     */
    public String convLanguage(String context,String type){
        if(type.toUpperCase().equals("ZH-CN")) {
            if (context.contains("License error")) {
                return "许可证错误,请检测许可证是否有效.";
            } else if (context.contains("License expired")) {
                return "许可证已过期.";
            } else if (context.contains("License test_date")) {
                return context.replace("License test_date", "许可证日期 ");
            }
        }

        return context;
    }

    /**
     * 验证密钥
     *
     * @return
     */
    public boolean verify() {

        errorMsg = "License error, please check whether the license is valid.";

        // 加载密钥
        String license = "license.dat";
        String currentCpuSN;
        BufferedInputStream bis = null;

        try {
            currentCpuSN = CpuSN.getCpuSN();
            bis = new BufferedInputStream(new FileInputStream(license));
            byte[] encodedData = new byte[1024];

            int bytesRead = 0;
            String strFileContents = "";
            while ((bytesRead = bis.read(encodedData)) != -1) {
                strFileContents += new String(encodedData, 0, bytesRead);
            }

            // 获取密钥
            String targetMap = RSAUtils.RsaDecryptCode(strFileContents);

            // 判断密钥
            if (StringUtils.isNotEmpty(targetMap)) {
                Map<String, String> target = JSON.parseObject(targetMap, HashMap.class);
                String product = target.get("product");
                String licenseSn = target.get("hardware");
                if (target.containsKey("outDate")) {
                    Date outDate = new Date(Long.valueOf(target.get("outDate")));

                    outDateString = DateUtil.formatDateTimeString(outDate);
                    if (DateUtil.compartDateTime(new Date(), outDate) > 0) {
                        //log.info("许可证已过期");
                        errorMsg = "License expired";
                        return false;
                    } else {
                        //log.info("许可证日期: "+outDateString);
                        //successMsg = "许可证日期: "+outDateString;
                        successMsg = "License test_date: " + outDateString;
                    }
                }

                if ("Catch".equals(product) && currentCpuSN.equals(licenseSn)) {
                    return true;
                }
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }

        return false;
    }
}
