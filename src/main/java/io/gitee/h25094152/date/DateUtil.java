package io.gitee.h25094152.date;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具
 * @author wh
 */
public class DateUtil {

    private final static String DateTimeFormatString = "yyyy-MM-dd HH:mm:ss";
    private final static String DateFormatString = "yyyy-MM-dd";
    private final static String TimeFormatString = "HH:mm:ss";

    private static SimpleDateFormat sdfDateTime = new SimpleDateFormat(DateTimeFormatString);
    private static SimpleDateFormat sdfDate = new SimpleDateFormat(DateFormatString);
    private static SimpleDateFormat sdfTime = new SimpleDateFormat(TimeFormatString);

    public static Date formatDateTimeStringToDate(String dateTime) throws ParseException {
        Date parse = sdfDateTime.parse(dateTime);

        return parse;
    }

    public static Date formatDateStringToDate(String dateTime) throws ParseException {
        Date parse = sdfDate.parse(dateTime);

        return parse;
    }


    /**
     * 格式化日期时间
     *
     * @param date
     * @return
     */
    public static String formatDateTimeString(Date date) {
        String format = sdfDateTime.format(date);
        return format;
    }

    /**
     * 获取当前时间
     *
     * @return
     */
    public static String getCurrentDateTimeString() {
        return formatDateTimeString(new Date());
    }

    /**
     * 格式化日期和时间
     *
     * @param date
     * @return
     */
    public static Date formatDateTimeToDate(Date date) throws ParseException {
        String format = sdfDateTime.format(date);
        Date parse = sdfDate.parse(format);

        return parse;
    }


    /**
     * 格式化日期
     *
     * @param date
     * @return
     */
    public static Date formatDateTimeToDateTime(Date date) {
        String format = sdfDate.format(date);
        Date parse = null;
        try {
            parse = sdfDate.parse(format);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parse;
    }

    /**
     * 格式化日期为字符串
     *
     * @param date
     * @return
     */
    public static String formatDateString(Date date) {
        String format = sdfDate.format(date);
        return format;

    }

    public static String formatTimeString(Date date) {
        String format = sdfTime.format(date);
        return format;

    }


    public static String calcDateTimeString(Date date, Integer Calendar, int dateNum) {
        return DateUtil.formatDateTimeString(DateUtil.calcDateTime(date, Calendar, dateNum));
    }

    public static String calcDateString(Date date, Integer Calendar, int dateNum) {
        return DateUtil.formatDateString(DateUtil.calcDateTime(date, Calendar, dateNum));
    }


    public static Date calcDate(Date date, Integer Calendar, int dateNum) throws ParseException {

        return DateUtil.formatDateTimeToDate(DateUtil.calcDateTime(date, Calendar, dateNum));
    }

    /**
     * 计算日期
     *
     * @param dateType
     * @param dateNum
     * @return
     */
    public static Date calcDateTime(Date date, Integer dateType, int dateNum) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        Date start = null;
        // 正数往后推，负数往前推

        if (Calendar.DATE == dateType) {
            //前一天
            cal.add(Calendar.DATE, dateNum);
            start = cal.getTime();
        } else if (Calendar.MONTH == dateType) {
            //前一月
            cal.add(Calendar.MONTH, dateNum);
            start = cal.getTime();
        } else if (Calendar.YEAR == dateType) {
            //前一月
            cal.add(Calendar.YEAR, dateNum);
            start = cal.getTime();
        } else if (Calendar.HOUR == dateType) {
            //前一小时
            cal.add(Calendar.HOUR_OF_DAY, dateNum);
            start = cal.getTime();
        } else if (Calendar.MINUTE == dateType) {
            //前一分钟
            cal.add(Calendar.MINUTE, dateNum);
            start = cal.getTime();
        } else if (Calendar.SECOND == dateType) {
            //前一秒
            cal.add(Calendar.SECOND, dateNum);
            start = cal.getTime();
        }
        return start;
    }

    /**
     * 比较日期 - 日期字符串格式
     *
     * @param sourceDate
     * @param targetDate
     * @return
     * @throws ParseException
     */
    public static int compartDateString(String sourceDate, String targetDate) throws ParseException {
        return compartDateTime(DateUtil.formatDateStringToDate(sourceDate), DateUtil.formatDateStringToDate(targetDate));
    }

    /**
     * 比较日期 - 日期时间字符串格式
     *
     * @param sourceDate
     * @param targetDate
     * @return
     * @throws ParseException
     */
    public static int compartDateTimeString(String sourceDate, String targetDate) throws ParseException {
        return compartDateTime(DateUtil.formatDateTimeStringToDate(sourceDate), DateUtil.formatDateTimeStringToDate(targetDate));
    }

    /**
     * 比较日期 - 日期时间格式
     * 返回值 0    c1 == c2
     * 返回值 1    c1 > c2
     * 返回值 -1   c1 < c2
     */
    public static int compartDateTime(Date sourceDate, Date targetDate) {

        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        int ret = 0;
        c1.setTime(sourceDate);
        c2.setTime(targetDate);
        int result = c1.compareTo(c2);
        if (result == 0) {
            //System.out.println("c1相等c2");
            ret = 0;
        } else if (result < 0) {
            //System.out.println("c1小于c2");
            ret = -1;
        } else {
            //System.out.println("c1大于c2");
            ret = 1;
        }

        return ret;
    }


    public static int differentDateString(String date1, String date2, Integer calendar) throws ParseException {
        return differentDaysByMillisecond(DateUtil.formatDateTimeStringToDate(date1), DateUtil.formatDateTimeStringToDate(date2),calendar);
    }

    /**
     * 通过时间秒毫秒数判断两个时间的间隔
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int differentDaysByMillisecond(Date date1, Date date2 , Integer dateType) {

        int dayType = 0 ;
        if (Calendar.DATE == dateType) {
            //前一天
            dayType = (1000 * 60 * 60 * 24);
        } else if (Calendar.MONTH == dateType) {
            //前一月
            dayType = (1000 * 60 * 60 * 24 * 365 );
        } else if (Calendar.YEAR == dateType) {
            //前一月
            dayType = (1000 * 60 * 60 * 24 * 30 );
        } else if (Calendar.HOUR == dateType) {
            //前一小时
            dayType = (1000 * 60 * 60 );
        } else if (Calendar.MINUTE == dateType) {
            //前一分钟
            dayType = (1000 * 60);
        }else if (Calendar.SECOND == dateType) {
            //前一秒
            dayType = (1000 );
        }

        int days = (int) ((date2.getTime() - date1.getTime()) / dayType);
        return days;
    }

    public static void main(String[] args) throws ParseException {

        //当前时间
        Date currentDate = new Date();
        System.out.println(formatDateString(currentDate));
        System.out.println(formatTimeString(currentDate));


        System.out.println("date = "+formatDateTimeString(formatDateTimeToDate(currentDate)));



        //当前时间
        System.out.println(DateUtil.formatDateTimeString(currentDate));

        System.out.println("-------");

        // 时间加减
        String date1 = DateUtil.calcDateTimeString(new Date(), Calendar.MINUTE, -5);
        System.out.println("date1 = " + date1);

        // 时间加减
        String date2 = DateUtil.calcDateTimeString(new Date(), Calendar.SECOND, 1);
        System.out.println("date2 = " + date2);

        // 比较两个时间大小
        System.out.println(compartDateTimeString(date2, date1));

        System.out.println("1-----1");
        System.out.println(differentDateString("2020-10-10 09:39:21", "2020-10-10 15:19:12",Calendar.MINUTE));

        // 获取两个时间差
        int i = differentDateString(date1, date2,Calendar.SECOND);
        System.out.println("test_date i = " + i);
    }


}
