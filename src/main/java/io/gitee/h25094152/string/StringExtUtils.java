package io.gitee.h25094152.string;


import org.apache.commons.lang3.StringUtils;

/**
 * 驼峰 互转 下划线
 */
public class StringExtUtils {

    /**
     * 分割线
     *
     * @param msg
     */
    public static void splitLine(String msg) {
        System.out.println("-----------------" + msg + "-----------------");
    }

    /**
     * 分割线
     *
     * @param msg
     */
    public static void splitLineTow(String msg) {
        System.out.println("\r\n-----------------" + msg + "-----------------");
    }

    /**
     * 功能：驼峰命名转下划线命名
     * 小写和大写紧挨一起的地方,加上分隔符,然后全部转小写
     */
    public static String camel2under(String c) {
        String separator = "_";
        c = c.replaceAll("([a-z])([A-Z])", "$1" + separator + "$2").toLowerCase();
        return c;
    }

    /**
     * 功能：下划线命名转驼峰命名
     * 将下划线替换为空格,将字符串根据空格分割成数组,再将每个单词首字母大写
     *
     * @param s
     * @return
     */
    private static String under2camel(String s) {
        String separator = "_";
        String under = "";
        s = s.toLowerCase().replace(separator, " ");
        String sarr[] = s.split(" ");
        for (int i = 0; i < sarr.length; i++) {
            String w = sarr[i].substring(0, 1).toUpperCase() + sarr[i].substring(1);
            under += w;
        }
        return under;
    }

    /**
     * 首字母大写
     *
     * @param s
     * @return
     */
    private static String namcFristCapUpper(String s) {

        return StringUtils.capitalize(s);
    }

    /**
     * 首字母小写
     *
     * @param s
     * @return
     */
    private static String namcFristCapDown(String s) {

        return StringUtils.uncapitalize(s);
    }


    public static void main(String[] args) {
        String c = "userId";
        System.out.println("驼峰命名转下划线命名" + StringExtUtils.camel2under(c));
        String s = "user_id";
        System.out.println("下划线命名转驼峰命名" + under2camel(s));


        System.out.println("----------------------");


        String name = "name";
        String password = "PassWord";
        System.out.println("namcFristCapUp = " + namcFristCapUpper(name));
        System.out.println("password = " + namcFristCapDown(password));
    }


}
